﻿namespace FadokoBackendV4.Models.DTOs
{
    public class CommodityRequestDto
    {
        public string CoName { get; set; }
        public int CoUnit { get; set; }
        public int CoCat { get; set; }
        public int CoPrice { get; set; }
        public int CoActive { get; set; }
    }
}
